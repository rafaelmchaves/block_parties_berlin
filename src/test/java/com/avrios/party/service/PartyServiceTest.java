package com.avrios.party.service;

import com.avrios.party.client.PartyClient;
import com.avrios.party.client.PartyDTO;
import com.avrios.party.repository.PartyRepository;
import com.avrios.party.repository.entities.Party;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PartyServiceTest {

	private PartyService partyService;

	@Mock
	private PartyRepository partyRepository;

	@Mock
	private PartyClient partyClient;

	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);

		partyService = new PartyService(partyClient, partyRepository);
	}

	@Test
	public void search_givenANameAndDate_returnResultOfTheFind() {
		//Given
		final String name = "Oster-Ritterfest auf der Zitadelle Spandau";
		final LocalDate localDate = LocalDate.of(2022, Month.JULY, 10);

		Mockito.when(this.partyRepository.findByNameAndDate(name, localDate)).thenReturn(getParties());

		//when
		var result = partyService.search(name, localDate);

		var argumentCaptorName = ArgumentCaptor.forClass(String.class);
		var argumentCaptorDate = ArgumentCaptor.forClass(LocalDate.class);

		Mockito.verify(this.partyRepository, Mockito.times(1))
			.findByNameAndDate(argumentCaptorName.capture(), argumentCaptorDate.capture());

		//then
		Assertions.assertAll(() ->
		{
			Assertions.assertEquals(1, result.size());
			Party party = result.stream().findFirst().get();
			Assertions.assertEquals(name, party.getName());
			Assertions.assertEquals(LocalDate.of(2022, Month.JULY, 9), party.getFrom());
			Assertions.assertEquals(LocalDate.of(2022, Month.JULY, 10), party.getUntil());

			Assertions.assertEquals(name, argumentCaptorName.getValue());
			Assertions.assertEquals(localDate, argumentCaptorDate.getValue());
		});

	}

	@Test
	public void savePartiesFromTheClient_givenPartiesFromClient_saveAllParties() {

		Mockito.when(this.partyClient.getAll()).thenReturn(getPartiesDTO());

		var argumentCaptorPartyDto = ArgumentCaptor.forClass(Party.class);

		this.partyService.savePartiesFromTheClient();

		Mockito.verify(this.partyRepository, Mockito.times(2)).save(argumentCaptorPartyDto.capture());

		var firstElement = argumentCaptorPartyDto.getAllValues().get(0);
		var secondElement = argumentCaptorPartyDto.getAllValues().get(1);

		Assertions.assertAll(() ->
		{
			Assertions.assertEquals("Oster-Ritterfest auf der Zitadelle Spandau", firstElement
				.getName());
			Assertions.assertEquals("KUNST trifft HANDWERK", secondElement
				.getName());
		});
	}

	@Test
	public void savePartiesFromTheClient_givenThereIsNoParty_noPartySaved() {

		Mockito.when(this.partyClient.getAll()).thenReturn(new ArrayList<>());

		this.partyService.savePartiesFromTheClient();

		Mockito.verify(this.partyRepository, Mockito.times(0));

	}

	private List<Party> getParties() {

		Party party = Party.builder()
			.name("Oster-Ritterfest auf der Zitadelle Spandau")
			.from(LocalDate.of(2022, Month.JULY, 9))
			.until(LocalDate.of(2022, Month.JULY, 10))
			.build();

		return Collections.singletonList(party);
	}

	private List<PartyDTO> getPartiesDTO() {

		PartyDTO partyOne = PartyDTO.builder()
			.name("Oster-Ritterfest auf der Zitadelle Spandau")
			.from(LocalDate.of(2022, Month.JULY, 9))
			.until(LocalDate.of(2022, Month.JULY, 10))
			.build();

		PartyDTO partyTwo = PartyDTO.builder()
			.name("KUNST trifft HANDWERK")
			.from(LocalDate.of(2022, Month.OCTOBER, 9))
			.until(LocalDate.of(2022, Month.OCTOBER, 10))
			.build();

		final List<PartyDTO> parties = new ArrayList<>();
		Collections.addAll(parties, partyOne, partyTwo);

		return parties;
	}
}

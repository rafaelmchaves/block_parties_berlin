package com.avrios.party.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.time.LocalDate;
import java.time.Month;

public class PartyCustomRepositoryImplTest {

	private PartyCustomRepositoryImpl partyCustomRepository;

	@Mock
	private MongoTemplate mongoTemplate;


	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);

		partyCustomRepository = new PartyCustomRepositoryImpl(mongoTemplate);
	}

	@Test
	public void findByNameAndDate_nameAndDateIsNull_returnQueryWithoutCriteria() {
		partyCustomRepository.findByNameAndDate(null, null);

		var queryCaptor = ArgumentCaptor.forClass(Query.class);
		Mockito.verify(mongoTemplate).find(queryCaptor.capture(), Mockito.any());

		Assertions.assertEquals(0, queryCaptor.getValue().getQueryObject().size());

	}

	@Test
	public void findByNameAndDate_dateIsNull_returnQueryWithoutCriteria() {
		partyCustomRepository.findByNameAndDate("Oster-Ritterfest auf der Zitadelle Spanda", null);

		var queryCaptor = ArgumentCaptor.forClass(Query.class);
		Mockito.verify(mongoTemplate).find(queryCaptor.capture(), Mockito.any());
		Assertions.assertAll(() -> {
			Assertions.assertEquals(1, queryCaptor.getValue().getQueryObject().size());
			Assertions.assertEquals("Oster-Ritterfest auf der Zitadelle Spanda",
				queryCaptor.getValue().getQueryObject().get("name").toString());
		});
	}

	@Test
	public void findByNameAndDate_nameAndDateIsNotNull_returnQueryWithoutCriteria() {
		partyCustomRepository.findByNameAndDate("Oster-Ritterfest auf der Zitadelle Spanda", LocalDate.of(2022, Month.JULY, 11));

		var queryCaptor = ArgumentCaptor.forClass(Query.class);
		Mockito.verify(mongoTemplate).find(queryCaptor.capture(), Mockito.any());

		Assertions.assertAll(() -> {

			Assertions.assertEquals(3, queryCaptor.getValue().getQueryObject().size());
			Assertions.assertEquals("Oster-Ritterfest auf der Zitadelle Spanda",
				queryCaptor.getValue().getQueryObject().get("name").toString());
			Assertions.assertEquals("Document{{$lte=2022-07-11}}",
				queryCaptor.getValue().getQueryObject().get("from").toString());
			Assertions.assertEquals("Document{{$gte=2022-07-11}}",
				queryCaptor.getValue().getQueryObject().get("until").toString());
		});

	}

	@Test
	public void findByNameAndDate_nameIsNull_returnQueryWithoutCriteria() {
		partyCustomRepository.findByNameAndDate(null, LocalDate.of(2022, Month.JULY, 11));

		var queryCaptor = ArgumentCaptor.forClass(Query.class);
		Mockito.verify(mongoTemplate).find(queryCaptor.capture(), Mockito.any());

		Assertions.assertAll(() -> {

			Assertions.assertEquals(2, queryCaptor.getValue().getQueryObject().size());
			Assertions.assertEquals("Document{{$lte=2022-07-11}}",
				queryCaptor.getValue().getQueryObject().get("from").toString());
			Assertions.assertEquals("Document{{$gte=2022-07-11}}",
				queryCaptor.getValue().getQueryObject().get("until").toString());

		});

	}

}

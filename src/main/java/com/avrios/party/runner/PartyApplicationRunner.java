package com.avrios.party.runner;

import com.avrios.party.service.PartyService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class PartyApplicationRunner implements ApplicationRunner {

	private final PartyService partyService;

	@Override
	public void run(ApplicationArguments args) {

		this.partyService.savePartiesFromTheClient();

	}
}

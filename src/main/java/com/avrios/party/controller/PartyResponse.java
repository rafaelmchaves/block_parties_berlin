package com.avrios.party.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Builder
public class PartyResponse {

	private String id;

	@Schema(description = "name or title of the party", example = "Oster-Ritterfest auf der Zitadelle Spanda")
	private String name;

	@Schema(description = "description of the party", example = "Oster-Ritterfest auf der Zitadelle Spanda")
	private String description;

	@Schema(description = "completed address", example = "Am Juliusturm 64")
	private String address;

	@Schema(description = "post code", example = "13599")
	private String postCode;

	@Schema(description = "district of the celebration", example = "Spandau")
	private String district;

	@Schema(description = "Informations about the organizer of the party", example = "Carnica Historische Feste & Märkte, Tel.: 0171/515 32 70, Fax: 033602/55 40 26")
	private String organizer;

	@Schema(description = "first day of the party", example = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate from;

	@Schema(description = "last day of the party", example = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate until;

	@Schema(description = "description about the duration of the party in each day", example = "10:00-21:00")
	private String timeDescription;

	@Schema(description = "website where you can find more informations", example = "www.carnica-spectaculi.de")
	private String website;

	@Schema(description = "email of the organization", example = "post@carnica.de")
	private String email;

	@Schema(description = "all important informations about the party", example = "Kinder bis 5 Jahre und Ritter in" +
		" Vollrüstung frei, Erwachsene 12 €, Schüler, Studenten & Gewandete 9 €, Knder 6 - 16 Jahre 6 €, Familie 30 € " +
		"(2 Erwachsene mit allen eigenen Kindern/Enkeln bis 16 Jahre)")
	private String observation;

	@Schema(description = "copyright", example = "Carnica")
	private String copyright;

	@Schema(description = "Url of the main image of the party", example = "/sen/web/service/maerkte-feste/strassen-volksfeste/assets/ritterfest500.jpg")
	private String imageUrl;

}

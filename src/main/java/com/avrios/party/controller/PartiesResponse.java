package com.avrios.party.controller;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class PartiesResponse {

	@Schema(description = "count of parties found.", example = "5")
	private Integer count;

	private List<PartyResponse> parties;

}

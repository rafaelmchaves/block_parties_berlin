package com.avrios.party.controller;

import com.avrios.party.repository.entities.Party;
import com.avrios.party.service.PartyService;
import io.swagger.v3.oas.annotations.OpenAPI30;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/parties")
@RequiredArgsConstructor
@OpenAPIDefinition(info = @Info(title = "Party documentation")
)
public class PartyController {

	private final PartyService partyService;

	@GetMapping
	@Operation(description = "Get all parties in the city. You can use filters by name and date of the party" +
		"(it will be searched in the range of the party in cases that the block party is not one day of celebration)")
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "Parties returned")
	})
	public ResponseEntity<PartiesResponse> getParties(@Parameter(description = "Entire or part of the name of the party.", example = "Festival")
														  @RequestParam(required = false) String name,
													  @Parameter(description = "Date of the party", example = "yyyy-MM-dd")
													  @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {

		var parties = this.partyService.search(name, date);

		return ResponseEntity.ok(buildPartiesResponse(parties));

	}

	private PartiesResponse buildPartiesResponse(List<Party> parties) {
		return PartiesResponse
			.builder()
			.count(parties.size())
			.parties(buildPartyResponse(parties))
			.build();
	}

	private List<PartyResponse> buildPartyResponse(List<Party> parties) {
		return parties.stream().map(party -> PartyResponse
			.builder()
			.name(party.getName())
			.description(party.getDescription())
			.organizer(party.getOrganizer())
			.address(party.getObservation())
			.district(party.getDistrict())
			.postCode(party.getPostCode())
			.observation(party.getObservation())
			.copyright(party.getCopyright())
			.from(party.getFrom())
			.until(party.getUntil())
			.timeDescription(party.getTimeDescription())
			.website(party.getWebsite())
			.email(party.getEmail())
			.imageUrl(party.getImageUrl())
			.build()
		).collect(Collectors.toList());
	}
}

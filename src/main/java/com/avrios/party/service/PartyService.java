package com.avrios.party.service;

import com.avrios.party.client.PartyClient;
import com.avrios.party.client.PartyDTO;
import com.avrios.party.repository.entities.Party;
import com.avrios.party.repository.PartyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PartyService {

	private final PartyClient partyClient;

	private final PartyRepository partyRepository;

	public void savePartiesFromTheClient() {

		var parties = searchAllPartiesFromClient();
		partyRepository.deleteAll();
		Flux.fromStream(parties.parallelStream())
			.subscribe(
				partyDTO -> partyRepository.save(buildParty(partyDTO)),
				throwable -> log.error("It was not possible to save parties {}", throwable.toString()),
				() -> log.info("Parties saved")
			);

	}

	private Party buildParty(PartyDTO partyDTO) {
		return Party
			.builder()
			.name(partyDTO.getName())
			.description(partyDTO.getDescription())
			.organizer(partyDTO.getOrganizer())
			.address(partyDTO.getObservation())
			.district(partyDTO.getDistrict())
			.postCode(partyDTO.getPostCode())
			.observation(partyDTO.getObservation())
			.copyright(partyDTO.getCopyright())
			.from(partyDTO.getFrom())
			.until(partyDTO.getUntil())
			.timeDescription(partyDTO.getTimeDescription())
			.website(partyDTO.getWebsite())
			.email(partyDTO.getEmail())
			.imageUrl(partyDTO.getImageUrl())
			.build();
	}

	public List<Party> search(String name, LocalDate date) {
		return this.partyRepository.findByNameAndDate(name, date);
	}

	private List<PartyDTO> searchAllPartiesFromClient() {
		return partyClient.getAll();
	}

}

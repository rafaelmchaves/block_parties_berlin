package com.avrios.party.client;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class PartyClient {

	private static final String URL = "https://www.berlin.de/sen/web/service/maerkte-feste/strassen-volksfeste/index.php/index/all.json?q";
	private final RestTemplate restTemplate;

	public List<PartyDTO> getAll() {

		var response = this.restTemplate.exchange(URL, HttpMethod.GET,
			null, PartyClientResponse.class);

		return Objects.requireNonNull(response.getBody()).getIndex();
	}

}

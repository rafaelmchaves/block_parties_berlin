package com.avrios.party.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
public class PartyDTO {

	@JsonProperty("rss_titel")
	private String name;

	@JsonProperty("bezeichnung")
	private String description;

	@JsonProperty("strasse")
	private String address;

	@JsonProperty("plz")
	private String postCode;

	@JsonProperty("bezirk")
	private String district;

	@JsonProperty("veranstalter")
	private String organizer;

	@JsonProperty("von")
	private LocalDate from;

	@JsonProperty("bis")
	private LocalDate until;

	@JsonProperty("zeit")
	private String timeDescription;

	@JsonProperty("www")
	private String website;

	@JsonProperty("mail")
	private String email;

	@JsonProperty("bemerkungen")
	private String observation;

	@JsonProperty("copyright")
	private String copyright;

	@JsonProperty("bild")
	private String imageUrl;

}

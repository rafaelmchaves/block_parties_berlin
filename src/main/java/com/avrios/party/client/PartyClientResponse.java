package com.avrios.party.client;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class PartyClientResponse implements Serializable {

	private List<PartyDTO> index;

}

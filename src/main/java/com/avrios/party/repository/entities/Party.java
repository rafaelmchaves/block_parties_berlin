package com.avrios.party.repository.entities;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Builder
@Document("parties")
public class Party implements Serializable {

	@Id
	private String id;

	@Indexed
	private String name;

	private String description;

	private String address;

	private String postCode;

	private String district;

	private String organizer;

	@Indexed
	private LocalDate from;

	@Indexed
	private LocalDate until;

	private String timeDescription;

	private String website;

	private String email;

	private String observation;

	private String copyright;

	private String imageUrl;

}

package com.avrios.party.repository;

import com.avrios.party.repository.entities.Party;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PartyRepository  extends MongoRepository<Party, String>, PartyCustomRepository {

//    @Query(value = "select p from Party p where (:name is not null and name like '%:name%') and (:date  is not null and :date >= from and :date <= until)", name = "name")
//	List<Party> findNameAndDate(String name, LocalDate date);

}

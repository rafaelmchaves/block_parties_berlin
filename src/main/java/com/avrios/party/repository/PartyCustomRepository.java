package com.avrios.party.repository;

import com.avrios.party.repository.entities.Party;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
@Repository
public interface PartyCustomRepository {

	List<Party> findByNameAndDate(String name, LocalDate date);

}

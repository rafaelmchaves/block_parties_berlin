package com.avrios.party.repository;


import java.time.LocalDate;
import java.util.List;

import com.avrios.party.repository.entities.Party;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@RequiredArgsConstructor
public class PartyCustomRepositoryImpl implements PartyCustomRepository {

	private final MongoTemplate mongoTemplate;

	@Override
	public List<Party> findByNameAndDate(String name, LocalDate date) {

		final Query query = new Query();

		if(name!= null) {
			query.addCriteria(Criteria.where("name").regex(name));
		}
		if(date != null) {
			query.addCriteria(Criteria.where("from").lte(date));
			query.addCriteria(Criteria.where("until").gte(date));
		}

		return mongoTemplate.find(query, Party.class);
	}

}

# About

Avrios loves a good block party. One of the great things about Berlin is
that the city releases block parties and fairs as open data, so we can
discover new events and make sure we never miss a good one. Your
assignment is to make it possible for us to find those events no matter
where in the city it takes us.

I created an API that returns a set of events. The application is consuming all parties from this api:

```
https://www.berlin.de/sen/web/service/maerkte-feste/strassen-volksfeste/index.php/index/all.json?q=
```

When the application starts, it consumes all parties found in this link above, and it saved everything in a mongodb database.

# Start up the Application

It's a spring boot application.
Run up a mongo database and set the values of mongo variables in application.properties. After it, run the PartyApplication.java

# Monitoring

The application is configured to be monitored by prometheus and actuator.

It was my first time that I used Prometheus and grafana, and this link helped me a lot:
https://www.techgeeknext.com/spring-boot/spring-boot-actuator-prometheus-grafana

prometheus.yml file that sets the configurations of the metrics:
```
global:
    scrape_interval: 15s # By default, scrape targets every 15 seconds.# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
    # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
    - job_name: 'prometheus'    # Override the global default and scrape targets from this job every 5 seconds.
      metrics_path: '/actuator/prometheus'
      scrape_interval: 5s
      static_configs:
          - targets: ['localhost:8080']

```

# Swagger

```
http://localhost:8080/swagger-ui/index.html#/
```

# Next steps

There are some features that I think that would be good to implement in the future:

- MemoryCache: Because parties and informations of the parties don't change every day. 
So, a cache would be good for the cost of the apllication and reduce the access in the mongo. It would be interesting if
we create a schedule that deletes and insert all parties everyday.

- Possibility of creating new parties. So, create an API that saves parties.

- Favorite list of parties. So, if I want to go to a party, I put this party in my favorite list and receive an email 
some days before the beginning of the event remind me about it.

- Create an integration test with embedded mongo.

- Create a pipeline to deploy the application automatically.
